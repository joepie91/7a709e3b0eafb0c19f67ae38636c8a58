# Essential utilities

* __[`default-value`](https://www.npmjs.com/package/default-value)__: Returns either the value that's passed in, or a 'default value' if there isn't one. Useful for dealing with options.

# Working with data

## Type checking

* __[`is-array`](https://www.npmjs.com/package/is-array)__: Checks whether a value is an array.
* __[`is-stream`](https://www.npmjs.com/package/is-stream)__: Checks whether a value is a stream of some sort.
* __[`type-of-is`](https://www.npmjs.com/package/type-of-is)__: Various utilities for checking and determining types.

## Arrays

* __[`in-array`](https://www.npmjs.com/package/in-array)__: Checks whether an array contains a particular value.
* __[`arr-flatten`](https://www.npmjs.com/package/arr-flatten)__: Turns nested arrays into a single 'flat' array.
* __[`arr-union`](https://www.npmjs.com/package/arr-union)__: Combines a list of arrays, ensuring only unique values
* __[`assure-array`](https://www.npmjs.com/package/assure-array)__: Wraps a value in an array if it isn't already one.
* __[`array-unique`](https://www.npmjs.com/package/array-unique)__: Removes duplicate values from an array.

## Objects

* __[`xtend`](https://www.npmjs.com/package/xtend)__: Combines multiple objects into one, without modifying any of the originals.
* __[`object.pick`](https://www.npmjs.com/package/object.pick)__: Returns a copy of an object, containing *only* the specified properties.
* __[`object.omit`](https://www.npmjs.com/package/object.omit)__: Returns a copy of an object, containing everything *except* for the specified properties.

<!-- * __[`copy-properties`](https://www.npmjs.com/package/copy-properties)__: Copies a specific set of properties from one object to another (if they are set). -->

## Strings

* __[`repeat-string`](https://www.npmjs.com/package/repeat-string)__: Returns a given string, but repeated N times.

# Promises

## ES6 Promises

(Seriously though, you should probably be using [Bluebird](http://bluebirdjs.com/docs/getting-started.html) instead. It's more robust and has these things built in.)

* __[`es6-promise-try`](https://www.npmjs.com/package/es6-promise-try)__: An implementation of [Promise.try](http://bluebirdjs.com/docs/api/promise.try.html) for ES6 Promises.
* __[`es6-promisify`](https://www.npmjs.com/package/es6-promisify)__: Converts a Node.js-callback-style function into one that returns an ES6 Promise. Equivalent to [Promise.promisify](http://bluebirdjs.com/docs/api/promise.promisify.html).
* __[`es6-promisify-all`](https://www.npmjs.com/package/es6-promisify-all)__: Like `es6-promisify`, but converts an entire module or prototype. Equivalent to [Promise.promisifyAll](http://bluebirdjs.com/docs/api/promise.promisifyall.html).

## Control flow

* __[`promise-task-queue`](https://www.npmjs.com/package/promise-task-queue)__: A configurable task queue that supports Promises, with support for rate limiting and concurrency control.
* __[`promise-while-loop`](https://www.npmjs.com/package/promise-while-loop)__: An asynchronous while-loop implementation with full support for Promises.

## Strange promisification cases

* __[`promise-simple-callback`](https://www.npmjs.com/package/promise-simple-callback)__: Promisifies a function that expects a callback with a single (result) argument, rather than the usual `(err, result)` style callbacks.

# Security

## Password hashing

You should use either of the following two. Note that while `scrypt` is newer and thus less tested, `bcrypt` has a limit of around 72 characters for the password. __Do not use pure-JavaScript implementations!__ If you are having trouble installing these, refer to [this page](https://gist.github.com/joepie91/95ed77b71790442b7e61#setting-up-your-environment).

* __[`scrypt-for-humans`](https://www.npmjs.com/package/scrypt-for-humans)__: Human-friendly API for using `scrypt`.
* __[`bcrypt`](https://www.npmjs.com/package/bcrypt)__: Bindings for `bcrypt`.

## Random values

__Don't use `Math.random`, or anything based on it!__ In most cases (API keys, reset tokens, lottery numbers, ...) you actually need *cryptographically secure* random values, and `Math.random` [can't provide these](https://github.com/XMPPwocky/nodebeefcl).

The only case in which you want to use `Math.random`, is when you need a *lot* of random numbers very quickly and it doesn't matter whether they are predictable - for example, when spawning monsters in a game at random locations.

* __[`random-number-csprng`](https://www.npmjs.com/package/random-number-csprng)__: A cryptographically secure generator for random numbers in a range.

## Cryptographical tokens

* __[`jsonwebtoken-promisified`](https://www.npmjs.com/package/jsonwebtoken-promisified)__: Library for working with JSON Web Tokens (JWT), supporting Promises out of the box.

# Streams

## Custom streams

* __[`through2`](https://www.npmjs.com/package/through2)__: Easily create custom streams, including object streams.

## Common transform streams

* __[`dev-null`](https://www.npmjs.com/package/dev-null)__: Writable stream that throws away whatever you put into it, without looking at it. Useful for 'draining' a stream.
* __[`through2-sink`](https://www.npmjs.com/package/through2-sink)__: Stream that calls a callback for every write, and then throws away the data.
* __[`through2-spy`](https://www.npmjs.com/package/through2-spy)__: Stream that calls a callback for every write, and then passes through the original data.

## Other streaming usecases

* __[`stream-combiner2`](https://www.npmjs.com/package/stream-combiner2)__: Represents a series of streams that pipe into each other, as a single stream. Useful for abstracting out pipelines.
* __[`combined-stream2`](https://www.npmjs.com/package/combined-stream2)__: Provides a single Readable stream that automatically concatenates the contents of multiple other streams or Buffers, in order.
* __[`concat-stream`](https://www.npmjs.com/package/concat-stream)__: Buffers up a stream, and calls a callback with the concatenated result when the stream has finished.

## Stream utilities

* __[`stream-length`](https://www.npmjs.com/package/stream-length)__: Attempts to obtain the total length of a stream, without reading out its contents.

# Multimedia

## Browser codecs

* __[aac](https://www.npmjs.com/package/aac)__: Pure-JS AAC decoder for browsers.
* __[alac](https://www.npmjs.com/package/alac)__: Pure-JS ALAC decoder for browsers.
* __[broadway-player](https://www.npmjs.com/package/broadway-player)__: Pure-JS H.264 decoder for browsers. This is an unofficial build of [Broadway.js](https://github.com/mbebenita/Broadway), and [may be moved in the future](https://github.com/mbebenita/Broadway/pull/113).
* __[flac.js](https://www.npmjs.com/package/flac.js)__: Pure-JS FLAC decoder for browsers.
* __[hls.js](https://www.npmjs.com/package/hls.js)__: HLS polyfill for browsers that do not natively support it.
* __[mp3](https://www.npmjs.com/package/mp3)__: Pure-JS MP3 decoder for browsers.
* __[opus.js](https://www.npmjs.com/package/opus.js)__: An Emscripten-compiled Opus decoder that works in the browser.